package sondow.twitter;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.Configuration;

/**
 * Interacts with the twitter API to perform twitter actions for a given Twitter account.
 *
 * @author @JoeSondow
 */
public class Updater {

    private Twitter twitter;

     /**
     * Creates an instance to interact with the Twitter account specified by the supplied
     * configuration.
     *
     * @param configuration the Twitter configuration to use to authenticate
     */
    public Updater(Configuration configuration) {
        this(new TwitterFactory(configuration).getInstance());
    }

    public Updater(Twitter twitter) {
        this.twitter = twitter;
    }

    /**
     * Posts a updateDisplayName.
     *
     * @param newName the new display name
     */
    public void updateDisplayName(String newName) {
        try {
            String screenName = twitter.getScreenName();
            User user = twitter.showUser(screenName);
            String description = user.getDescription();
            String url = user.getURL();
            String location = user.getLocation();
            String displayName = user.getName();
            twitter.updateProfile(newName, url, location, description);
            String msg = "Changed @" + screenName + " name from '" + displayName + "' to '" +
                    newName + "'";
            System.out.println(msg);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }
}
