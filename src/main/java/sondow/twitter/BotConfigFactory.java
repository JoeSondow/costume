package sondow.twitter;

import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Builds a BotConfig based on environment variables.
 */
public class BotConfigFactory {

    private Environment environment;

    public BotConfigFactory(Environment environment) {
        this.environment = environment;
    }

    public BotConfigFactory() {
        this(new Environment());
    }

    public BotConfig configure() {
        Configuration twitterConf = configureTwitter();
        String namesJsonUrl = environment.get("names_json_url");
        return new BotConfig(twitterConf, namesJsonUrl);
    }


    private Configuration configureTwitter() {
        String twitterAccount = environment.get("twitter_account");
        return configureTwitter(twitterAccount);
    }

    /**
     * AWS Lambda only allows underscores in environment variables, not dots, so the default ways
     * twitter4j finds keys aren't possible. Instead, this custom code gets the configuration either
     * from Lambda-friendly environment variables or else allows Twitter4J to look in its default
     * locations like twitter4j.properties file at the project root, or on the classpath, or in
     * WEB-INF.
     * <p>
     * The config from this method also includes keys for accessing the Twitter API, as well as
     * some domain-specific variables.
     *
     * @return configuration containing AWS and Twitter authentication strings and other variables
     */
    private Configuration configureTwitter(String account) {

        // Override with a specific account if available. This mechanism allows us to provide
        // multiple key sets in the AWS Lambda configuration, and switch which Twitter account
        // to target by retyping just the target Twitter account name in the configuration.
        ConfigurationBuilder configBuilder = new ConfigurationBuilder();
        String key = (account == null) ? "cred_twitter" : "cred_twitter_" + account;
        String credentialsCsv = environment.require(key);

        String[] tokens = credentialsCsv.split(",");
        String screenName = tokens[0];
        String consumerKey = tokens[1];
        String consumerSecret = tokens[2];
        String accessToken = tokens[3];
        String accessTokenSecret = tokens[4];

        configBuilder.setUser(screenName);
        configBuilder.setOAuthConsumerKey(consumerKey);
        configBuilder.setOAuthConsumerSecret(consumerSecret);
        configBuilder.setOAuthAccessToken(accessToken);
        configBuilder.setOAuthAccessTokenSecret(accessTokenSecret);

        return configBuilder.setTrimUserEnabled(true).setTweetModeExtended(true).build();
    }
}
