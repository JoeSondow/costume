package sondow.twitter;

import java.time.ZonedDateTime;

public class Time {

    public ZonedDateTime nowZonedDateTime() {
        return ZonedDateTime.now();
    }
}
