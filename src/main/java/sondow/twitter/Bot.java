package sondow.twitter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import twitter4j.conf.Configuration;

/**
 * The main application logic.
 */
public class Bot {

    /**
     * The full configuration for this bot.
     */
    private BotConfig botConfig;

    /**
     * The object that does the updating.
     */
    private Updater updater;

    /**
     * The provider of contents of remote text files.
     */
    private TextFileProvider textFileProvider;

    /**
     * Source of current time.
     */
    private Time time;

    /**
     * Constructs a new bot with the specified objects.
     */
    Bot(BotConfig config, Updater updater, TextFileProvider textFileProvider, Time time) {
        init(config, updater, textFileProvider, time);
    }

    /**
     * Constructs a new bot.
     */
    Bot() {
        BotConfig config = new BotConfigFactory().configure();
        Configuration twitterConfig = config.getTwitterConfig();
        init(config, new Updater(twitterConfig), new TextFileProvider(), new Time());
    }

    private void init(BotConfig botConfig, Updater updater, TextFileProvider textFileProvider,
            Time time) {
        this.botConfig = botConfig;
        this.updater = updater;
        this.textFileProvider = textFileProvider;
        this.time = time;
    }

    /**
     * Performs the application logic.
     */
    public void go() {

        ZonedDateTime now = time.nowZonedDateTime();
        String cacheBuster = "?cachebuster=" + now.toInstant().getEpochSecond();
        String json = textFileProvider.download(botConfig.getNamesJsonUrl() + cacheBuster);
        Map<String, String> datesToNames = new NamesFileParser().parseDatesToNamesJson(json);

        String monthDay = now.format(DateTimeFormatter.ofPattern("MMM d"));
        String name = datesToNames.get(monthDay);
        if (name == null) {
            name = datesToNames.get("default");
        }
        if (name == null) {
            System.out.println("No name default name found, and no name for " + monthDay);
        } else {
            updater.updateDisplayName(name);
        }
    }
}
