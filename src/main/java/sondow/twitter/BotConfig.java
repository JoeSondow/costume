package sondow.twitter;

import twitter4j.conf.Configuration;

/**
 * The place to store all the bot's configuration variables, mostly from environment variables read
 * in BotConfigFactory.
 */
public class BotConfig {

    /**
     * The configuration for Twitter.
     */
    private Configuration twitterConfig;

    /**
     * The url of the json file where the mapping of dates to names is.
     */
    private String namesJsonUrl;

    BotConfig(Configuration twitterConfig, String namesJsonUrl) {
        this.twitterConfig = twitterConfig;
        this.namesJsonUrl = namesJsonUrl;
    }

    public Configuration getTwitterConfig() {
        return twitterConfig;
    }


    public String getNamesJsonUrl() {
        return namesJsonUrl;
    }

}
