package sondow.twitter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import twitter4j.JSONException;
import twitter4j.JSONObject;

public class NamesFileParser {

    Map<String, String> parseDatesToNamesJson(String json) {
        Map<String, String> datesToNames = new HashMap<>();
        JSONObject obj;
        try {
            obj = new JSONObject(json);
            Iterator keys = obj.keys();
            while (keys.hasNext()) {
                String date = (String) keys.next();
                String name = obj.getString(date);
                datesToNames.put(date, name);
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return datesToNames;
    }
}
