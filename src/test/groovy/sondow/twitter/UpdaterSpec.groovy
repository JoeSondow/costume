package sondow.twitter

import spock.lang.Specification
import twitter4j.Twitter
import twitter4j.User

class UpdaterSpec extends Specification {

    def "should update profile with new display name"() {
        setup:
        Twitter twitter = Mock()
        Updater tweeter = new Updater(twitter)
        User user = Mock()
        String desc = 'I was working in my lab late one night.'
        String loc = 'Spooky castle'
        String url = 'https://rockyhorror.com'

        when:
        tweeter.updateDisplayName("Mr. Hyde")

        then:
        1 * twitter.getScreenName() >> 'DrJeckyll'
        1 * twitter.showUser('DrJeckyll') >> user
        1 * user.getName() >> 'Doctor Jeckyll'
        1 * user.getDescription() >> desc
        1 * user.getLocation() >> loc
        1 * user.getURL() >> url
        1 * twitter.updateProfile('Mr. Hyde', url, loc, desc)
        0 * _._
    }
}
