package sondow.twitter

import java.time.ZonedDateTime
import spock.lang.Specification

class BotSpec extends Specification {

    String halloweenDatesToNames = '{\n' +
            '  "default": "Joe Sondow",\n' +
            '  "Oct 1": "Joe Scream-dow 😱",\n' +
            '  "Oct 2": "Crow Sondow 🐦",\n' +
            '  "Oct 3": "Joe Skeleton 💀",\n' +
            '  "Oct 4": "Joe Sword-OW! ⚔",\n' +
            '  "Oct 5": "Boo Sondow 😧 ",\n' +
            '  "Oct 6": "Joe Haunt-dow 🏚",\n' +
            '  "Oct 7": "Joker Sondow 🃏",\n' +
            '  "Oct 8": "Joe Spawn-dow 😈",\n' +
            '  "Oct 9": "Joe Scorpion 🦂",\n' +
            '  "Oct 10": "Frankensondow 🔩",\n' +
            '  "Oct 11": "Joe Son-drown ⚓",\n' +
            '  "Oct 12": "Joe Scarecrow 👒",\n' +
            '  "Oct 13": "Twilight Zone-dow 🚪",\n' +
            '  "Oct 14": "Joe Crossbones ☠",\n' +
            '  "Oct 15": "Joe Sond-howl 🐺",\n' +
            '  "Oct 16": "Joe Tombstone ⚰️",\n' +
            '  "Oct 17": "Ogre Sondow 👹",\n' +
            '  "Oct 18": "Joe Spook-dow 👺",\n' +
            '  "Oct 19": "Joe Scareclown 🤡",\n' +
            '  "Oct 20": "Jolt Sondow ⚡",\n' +
            '  "Oct 21": "Joe Bone-dow 🍖 ",\n' +
            '  "Oct 22": "Joe Sand Out ⌛",\n' +
            '  "Oct 23": "Poe Sondow 🐀",\n' +
            '  "Oct 24": "Joe Zombie 🧟",\n' +
            '  "Oct 25": "Joe Sond-owl 🦉",\n' +
            '  "Oct 26": "Ghost Sondow 👻",\n' +
            '  "Oct 27": "Joe Sundown 🌅",\n' +
            '  "Oct 28": "Count Sond-ula 🧛‍♂️",\n' +
            '  "Oct 29": "Joe Shadow 👤",\n' +
            '  "Oct 30": "Sexy Joe Sondow Costume 👙",\n' +
            '  "Oct 31": "Joe Sondo\'lantern 🎃",\n' +
            '  "Nov 1": "Joe Son-death ⚱️"\n' +
            '}'

    def "go should make updater do an update"() {
        setup:
        BotConfig botConfig = Mock()
        Updater tweeter = Mock()
        TextFileProvider textFileProvider = Mock()
        Time time = Mock()
        Bot bot = new Bot(botConfig, tweeter, textFileProvider, time)
        String contents = halloweenDatesToNames
        ZonedDateTime now = ZonedDateTime.parse('2019-' + date + 'T01:02:03Z')

        when:
        bot.go()

        then:
        1 * botConfig.getNamesJsonUrl() >> 'https://gist.github.com/b'
        1 * textFileProvider.download('https://gist.github.com/b?cachebuster=' + epoch) >> contents
        1 * time.nowZonedDateTime() >> now
        1 * tweeter.updateDisplayName(name)
        0 * _._

        where:
        epoch        | date    | name
        '1569805323' | '09-30' | 'Joe Sondow'
        '1570928523' | '10-13' | 'Twilight Zone-dow 🚪'
        '1571878923' | '10-24' | 'Joe Zombie 🧟'
        '1572570123' | '11-01' | 'Joe Son-death ⚱️'
        '1572656523' | '11-02' | 'Joe Sondow'
    }
}
