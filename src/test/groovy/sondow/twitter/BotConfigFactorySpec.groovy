package sondow.twitter

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification
import twitter4j.conf.Configuration

class BotConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration from environment variable"() {
        setup:
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        BotConfigFactory factory = new BotConfigFactory(environment)
        String filler = Environment.SPACE_FILLER
        envVars.set("cred_twitter", "${filler}cartoons,bartsimpson,snaggletooth," +
                "fredflintstone,bugsbunny${filler}")
        envVars.set("names_json_url", "https://gist.github.com/blah")

        when:
        BotConfig overallConfig = factory.configure()
        Configuration twitterConfig = overallConfig.getTwitterConfig()
        String url = overallConfig.getNamesJsonUrl()

        then:
        with(twitterConfig) {
            OAuthConsumerKey == 'bartsimpson'
            OAuthConsumerSecret == 'snaggletooth'
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
        url == "https://gist.github.com/blah"
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
    }

    def "configure should populate a config from env var based on selected twitter account"() {
        setup:
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        BotConfigFactory factory = new BotConfigFactory(environment)
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_account", "JoeSondow")
        envVars.set("cred_twitter_JoeSondow", "${filler}cartoons,bartsimpson,snaggletooth," +
                "fredflintstone,bugsbunny${filler}")
        envVars.set("names_json_url", "https://gist.github.com/blah")

        when:
        BotConfig overallConfig = factory.configure()
        Configuration twitterConfig = overallConfig.getTwitterConfig()
        String url = overallConfig.getNamesJsonUrl()

        then:
        with(twitterConfig) {
            OAuthConsumerKey == 'bartsimpson'
            OAuthConsumerSecret == 'snaggletooth'
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
        url == "https://gist.github.com/blah"
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
    }
}
